<?php

namespace Drupal\snippet_manager\Tests;

/**
 * Tests the Snippet manager user interface.
 *
 * @group snippet_manager
 *
 * @TODO: Turn this to browser test case.
 */
class SnippetUiTest extends TestBase {

  /**
   * Tests snippet variable form.
   */
  protected function testSnippetVariableForm() {

    $this->drupalGet('admin/structure/snippet/alpha/edit/template');
    $this->assertLink(t('Add variable'));
    $this->assertLinkByHref('/admin/structure/snippet/alpha/edit/variable/add');

    $this->drupalGet('/admin/structure/snippet/alpha/edit/variable/add');
    $this->assertPageTitle(t('Add variable'));
    $this->assertText(t('Type of the variable'));
    $select_prefix = '//select[@name="plugin_id" and @required]';

    $select_axes[] = '/option[@selected and text()="- Select -"]';
    $select_axes[] = '/optgroup[@label="Condition"]/option[@value="condition:request_path" and text()="Request Path"]';
    $select_axes[] = '/optgroup[@label="Condition"]/option[@value="condition:current_theme" and text()="Current Theme"]';
    $select_axes[] = '/optgroup[@label="Condition"]/option[@value="condition:user_role" and text()="User Role"]';
    $select_axes[] = '/optgroup[@label="Entity"]/option[@value="entity:snippet" and text()="Snippet"]';
    $select_axes[] = '/optgroup[@label="Entity"]/option[@value="entity:user" and text()="User"]';
    $select_axes[] = '/optgroup[@label="Other"]/option[@value="text" and text()="Formatted text"]';
    $select_axes[] = '/optgroup[@label="Other"]/option[@value="url" and text()="Url"]';
    $select_axes[] = '/optgroup[@label="Other"]/option[@value="file" and text()="File"]';
    $select_axes[] = '/optgroup[@label="View"]/option[@value="view:user_admin_people" and text()="People"]';
    $select_axes[] = '/optgroup[@label="View"]/option[@value="view:who_s_new" and text()="Who\'s new"]';
    $select_axes[] = '/optgroup[@label="View"]/option[@value="view:who_s_online" and text()="Who\'s online block"]';

    foreach ($select_axes as $axis) {
      $this->assertByXpath($select_prefix . $axis);
    }

    $this->assertText(t('Name of the variable'));
    $this->assertByXpath('//input[@name="name" and @required]');

    $edit = [
      'plugin_id' => 'text',
      'name' => 'bar',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save and continue'));

    $this->assertPageTitle(t('Edit variable %variable', ['%variable' => $edit['name']]));

    $this->assertUrl('/admin/structure/snippet/alpha/edit/variable/bar/edit');

    $this->drupalGet('admin/structure/snippet/alpha/edit/template');

    $expected_header = [
      t('Name'),
      t('Type'),
      t('Plugin'),
      t('Operations'),
    ];

    foreach ($this->xpath('//main//table//th') as $key => $th) {
      if ($expected_header[$key] != $th) {
        $this->fail('Valid table header was found.');
        break;
      }
    }

    $variable_row_prefix = '//main//table/tbody/tr';

    $label = $this->xpath($variable_row_prefix . '/td[position() = 1]/a[@href="#snippet-edit-form" and text() = "bar"]');
    $this->assertTrue($label, 'Valid snippet variable label was found');

    $label = $this->xpath($variable_row_prefix . '/td[position() = 2 and text() = "String"]');
    $this->assertTrue($label, 'Valid snippet variable type was found');

    $label = $this->xpath($variable_row_prefix . '/td[position() = 3 and text() = "text"]');
    $this->assertTrue($label, 'Valid snippet variable plugin was found');

    $links_prefix = $variable_row_prefix . '/td[position() = 4]//ul[@class="dropbutton"]/li';

    $edit_link = $this->xpath($links_prefix . '/a[contains(@href, "/snippet/alpha/edit/variable/bar/edit") and text() = "Edit"]');
    $this->assertTrue($edit_link, 'Valid snippet variable edit link was found');

    $delete_link = $this->xpath($links_prefix . '/a[contains(@href, "/snippet/alpha/edit/variable/bar/delete") and text() = "Delete"]');
    $this->assertTrue($delete_link, 'Valid snippet variable delete link was found');

    $this->drupalGet('/admin/structure/snippet/alpha/edit/variable/not-existing-variable/edit');
    $this->assertResponse(404);
  }

  /**
   * Tests snippet variable delete form.
   */
  protected function testSnippetVariableDeleteForm() {
    $this->drupalGet('/admin/structure/snippet/alpha/edit/variable/foo/delete');
    $this->assertPageTitle(t('Are you sure you want to delete the variable %variable?', ['%variable' => 'foo']));
    $this->assertText('This action cannot be undone.');
    $this->assertLink(t('Cancel'));
    $this->assertLinkByHref('/admin/structure/snippet/alpha/edit');
    $this->drupalPostForm(NULL, [], t('Delete'));
    $this->assertStatusMessage(t('The variable has been removed.'));
    $this->assertUrl('/admin/structure/snippet/alpha/edit/template');
    $this->assertText(t('Variables are not configured yet.'));
  }

}
