<?php

namespace Drupal\snippet_manager\Plugin\SnippetVariable;

use Drupal\Core\Form\FormStateInterface;
use Drupal\snippet_manager\SnippetVariableBase;

/**
 * Provides formatted text variable type.
 *
 * @SnippetVariable(
 *   id = "mini_snippet",
 *   title = @Translation("Mini snippet"),
 *   category = @Translation("Other"),
 * )
 */
class MiniSnippet extends SnippetVariableBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['template'] = [
      '#title' => $this->t('Template'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['template'],
      '#required' => TRUE,
      '#attributes' => [
        'data-snippet-code' => TRUE,
        'data-btn-bold' => TRUE,
        'data-btn-italic' => TRUE,
        'data-btn-underline' => TRUE,
        'data-btn-strike-through' => TRUE,
        'data-btn-list-numbered' => TRUE,
        'data-btn-list-bullet' => TRUE,
        'data-btn-undo' => TRUE,
        'data-btn-redo' => TRUE,
        'data-btn-clear-formatting' => TRUE,
        'data-btn-enlarge' => TRUE,
        'data-btn-shrink' => TRUE,
        'data-mode-select' => TRUE,
        'data-mode' => 'html_twig',
      ],
    ];

    $form['#attached']['library'][] = 'snippet_manager/editor';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['template' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'inline_template',
      '#template' => $this->configuration['template'],
      '#context' => [],
    ];
  }

}
